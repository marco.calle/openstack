# Openstack
#### configuracion de VM
```
Hostname : openstack-queens
OS : CentOS 7 Minimal
Dirección IP : 192.168.0.35
Cores: 6 cores
Memoria RAM: 8GB
Constraseña asignada por PackStack=19780dd71a0c4652 
```
## Step 1:
### Verificar versión, Hostname y dirección IP
> cat /etc/redhat-release

> uname -a

> ip a

# Setear hostname
> echo "192.168.0.35 openstack.linuxtopic.com" >> /etc/hosts

## Step 2:
### Deshabilitar NetworkManager y Firewalld
> systemctl stop firewalld NetworkManager
> systemctl disable firewalld NetworkManager

### Deshabilitar SELinux
##### Temporal
> setenforce 0 

OR
##### Permanente
> sed -i s/^SELINUX=.*$/SELINUX=permissive/ /etc/selinux/config && setenforce 0

## Step 3:
### Update System and Install RDO ( RPM Distribution of Openstack ) Package
> yum update -y && yum install -y centos-release-openstack-queens && yum upgrade -y

## Step 4:
### Instalar Packstack : Es un programa que usa modulos Puppet para desplegar varias partes de OpenStack
> yum -y install openstack-packstack

## Step 5:
### Generar Packstack Answer File, el archivo sirve como template de configuración
> packstack --gen-answer-file=opnstacktest-answere-file.txt
> vi opnstacktest-answere-file.txt

##### Modificar opnstacktest-answere-file.txt con los siguientes valores
```
CONFIG_DEFAULT_PASSWORD=server

# Specify 'y' to install OpenStack Object Storage (swift). ['y', 'n']
CONFIG_SWIFT_INSTALL=n

# Specify 'y' to install OpenStack Metering (ceilometer). ['y', 'n']
CONFIG_CEILOMETER_INSTALL=n

# Specify 'y' to install OpenStack Telemetry Alarming (Aodh). Note
# Aodh requires Ceilometer to be installed as well. ['y', 'n']
CONFIG_AODH_INSTALL=n

# Specify 'y' to install Nagios to monitor OpenStack hosts. Nagios
# provides additional tools for monitoring the OpenStack environment.
# ['y', 'n']
CONFIG_NAGIOS_INSTALL=n

# Server on which to install OpenStack services specific to the
# controller role (for example, API servers or dashboard).
CONFIG_CONTROLLER_HOST=192.168.0.35

# List the servers on which to install the Compute service.
CONFIG_COMPUTE_HOSTS=192.168.0.35

# List of servers on which to install the network service such as
# Compute networking (nova network) or OpenStack Networking (neutron).
CONFIG_NETWORK_HOSTS=192.168.0.35

# Specify 'y' to provision for demo usage and testing. ['y', 'n']
CONFIG_PROVISION_DEMO=n
```

## Step 6:
### Instalar Openstack usando Answer File
> packstack --answer-file opnstacktest-answere-file.txt

# Configurar Networking
## Step 1:
### Verificar dirección IP
> ip a

### Copiar archivo de interface y editar

> cd /etc/sysconfig/network-scripts/

> cp ifcfg-enp0s3 ifcfg-br-ex

> vi /etc/sysconfig/network-scripts/ifcfg-br-ex

```
TYPE="OVSBridge"
BOOTPROTO="static"
DEFROUTE="yes"
PEERDNS="yes"
PEERROUTES="yes"
NAME="br-ex"
UUID="39bd5629-d3aa-43f1-b038-e947832a4a2a"
DEVICE="br-ex"
DEVICETYPE=ovs
ONBOOT="yes"
IPADDR="192.168.0.35"
PREFIX="24"
GATEWAY="192.168.0.1"
DNS1="8.8.8.8"
DNS2="8.8.4.4"
```

> vi /etc/sysconfig/network-scripts/ifcfg-enp0s3

```
TYPE="OVSPort"
DEVICETYPE="ovs"
OVS_BRIDGE="br-ex"
BOOTPROTO="static"
DEFROUTE="yes"
PEERDNS="yes"
PEERROUTES="yes"
NAME="enp0s3"
UUID="39bd5629-d3aa-43f1-b038-e947832a4a2a"
DEVICE="enp0s3"
ONBOOT="yes"
```
> systemctl restart network

### Verificar Status :
> ip a

## Step 2:
### Configurar Neutron Controller
> vi /etc/neutron/plugin.ini

```
type_drivers = vxlan,flat
flat_networks = *
```

### Reiniciar servicio neutron-server

> systemctl restart neutron-server.service

## Step 3:
### Map the Bridge to the physical network : Find bridge_mappings and add “physnet1:br-ex“
> vi /etc/neutron/plugins/ml2/openvswitch_agent.ini

```
bridge_mappings = physnet1:br-ex
```

### Reiniciar servicio neutron-openvswitch-agent
> systemctl restart neutron-openvswitch-agent

## Step 4:
### Enables the use of External provider networks and leave blank
> vi /etc/neutron/l3_agent.ini
```
external_network_bridge =
```
### Restart neutron-l3-agent
> systemctl restart neutron-l3-agent.service

## Step 5:
Modificar el contenido de /root/keystonerc_admin
```
unset OS_SERVICE_TOKEN
    export OS_USERNAME=admin
    export OS_PASSWORD='19780dd71a0c4652'
    export OS_REGION_NAME=RegionOne
    export OS_AUTH_URL=http://192.168.0.35:5000/v3
    export PS1='[\u@\h \W(keystone_admin)]\$ '

export OS_TENANT_NAME=admin
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_IDENTITY_API_VERSION=3
```

### Run source file
> source keystonerc_admin

## Step 6:
### Crear red para la conexión hacia internet
> neutron net-create linuxtopic-external --provider:network_type flat --provider:physical_network physnet1 --router:external=True --shared

## Step 7:
Ingresar a Dashboard (Horizon)
```
http://192.168.0.35/
Usuario: admin
Passwrod: clave generada en /root/keystonerc_admin
```

# Errores
### Instancias - Error al visualizar la instancia por vnc interno

#### Failed to connect to server (code: 1006)
> vi /etc/nova/nova.conf

vncserver_proxyclient_address= 192.168.0.35

> systemctl restart openstack-nova-compute.service

## Obtener imagenes
https://docs.openstack.org/image-guide/obtain-images.html

## Crear imagen desde ISO
https://galvarado.com.mx/post/openstack-convertir-im%C3%A1genes-iso-a-qcow2/


## Enlaces de referencia
https://www.linuxtopic.com/2018/05/OpenStack-Queen-Installation-tutorial.html
https://www.linuxtopic.com/2018/06/openstack-queen-network-configuration.html
https://www.linuxtopic.com/2018/06/openstack-queens-compute-configuration.html

https://www.linuxtechi.com/multiple-node-openstack-liberty-installation-on-centos-7-x/

### Instalacion de OpenStack por servicios
https://ilearnedhowto.wordpress.com/2019/02/25/how-to-install-openstack-rocky-part-1/
https://ilearnedhowto.wordpress.com/2019/03/25/how-to-install-openstack-rocky-part-2/
https://ilearnedhowto.wordpress.com/2019/04/10/how-to-install-openstack-rocky-part-3-final/

#### Scripts para instalación de nodo Controller y Compute
https://github.com/dealfonso/openstack-install
